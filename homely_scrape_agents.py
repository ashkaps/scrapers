import traceback
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import selenium
from selenium.webdriver.common.keys import Keys
import os
import shutil
import time
import glob
import csv
from pandas import read_csv
import re
from pymongo import UpdateOne
import requests
import uuid
import json
import datetime
import numpy as np
import pandas as pd
import csv
from multiprocessing import Pool
import multiprocessing
from pyppeteer import launch
import csv

browser_col = dict()
DEFAILT_DOWNLOAD_DIR = os.path.join(os.getcwd(), 'downloads')
def wait_for_page_load(driver):
    while True:
        x = driver.execute_script("return document.readyState")
        if x == "complete":
            return True
        else:
            yield False

def element_disp(driver, xpath):
    try:
        driver.implicitly_wait(1)
        eles = driver.find_elements_by_xpath(xpath)
        if len(eles) > 0:
            driver.implicitly_wait(10)
            return True
    except:
        pass
    finally:
        driver.implicitly_wait(60)

def get_chrome_options():
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-ssl-errors=yes')
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--allow-insecure-localhost') 
    options.add_argument('--allow-running-insecure-content') 
    options.add_argument("--start-maximized")
    # options.headless = True
    prefs = {"profile.default_content_settings.popups": 0,
             "download.default_directory": 
                        DEFAILT_DOWNLOAD_DIR,#IMPORTANT - ENDING SLASH V IMPORTANT
             "directory_upgrade": True}
    options.add_experimental_option("prefs", prefs)
    
    try:
        shutil.rmtree(DEFAILT_DOWNLOAD_DIR)
    except:
        pass
    try:
        os.mkdir(DEFAILT_DOWNLOAD_DIR)
    except:
        pass
    # options.add_argument('--headless')
    return options




file_name = 'Suburbs MASTER.xlsx'
df = pd.read_excel(file_name, sheet_name=None, engine="openpyxl")

driver = None
out = list()
fail = list()
processed = list()


def close_search(driver):
    try:
        driver.implicitly_wait(0)
        driver.find_element_by_xpath("//button[@title='Remove']").click()
    except:
        pass
    finally:
        driver.implicitly_wait(5)

async def extract(r, i):
    
    try:
        
        browser = await launch({'headless': True})
        page = await browser.newPage()
        await page.waitFor(3000)
        await page.goto('https://www.homely.com.au/',  timeout=60000)
        # print(r, flush=True)
        agents = await page.xpath("//span[@title='Agents']")
        await agents[0].click()
        try:
            search = await page.xpath("//div[.='Suburb or postcode']")
            await search[0].click()
        except:
            pass
        search = await page.xpath("//input[@type='search']")
        await search[0].click()
        # time.sleep(1)
        search = await page.xpath("//input[@type='search']")
        await search[0].type(r[0])

        time.sleep(2)
        results = await page.xpath("//li//button[@role='link' and contains(.,'{}')  and contains(.,'{}')]".format(str(r[0]), r[1].replace('.0','')))
        await results[0].click()

        search_1 = await page.xpath("//button[@aria-label='Search button']")
        resp = await search_1[0].click()
        await page.waitForNavigation()
        with open('homely_agents_output_new.csv','a') as f:
            writer = csv.writer(f)
            writer.writerow([r['Post Code'], r['State'], r[0], page.url])
    except Exception as e:
        print(r, flush=True)
        traceback.print_exc()
        pass

if __name__ == '__main__':
    # Python 3.7
    import asyncio
    # asyncio.run(main())
    tasks = list()
    urls = [r for i, r in df['Sheet1'].iterrows()]
    i = 1
    cnt = 1
    n = 5
    final = [urls[i * n:(i + 1) * n] for i in range((len(urls) + n - 1) // n )] 
    

    for l in final:
        print(str(i), flush=True)
        loop = asyncio.new_event_loop()
        tasks = list()
        j = 0
        for r in l:
            tasks.append(loop.create_task(extract(r, j)))
            j = j+ 1
        loop.run_until_complete(asyncio.wait(tasks))
        loop.close()
        i = i + 1




'''
7.52 - 112
7.55 - 132
'''